import { nav } from "./nav.js";
import { home } from "./home.js";
import { menu } from "./menu.js";
import { contact } from "./contact.js";

nav()
home()

   
document
.querySelectorAll(".navButton")
.forEach((element) =>
  element.addEventListener("click", () => pageSwitch(element))
);

function pageSwitch(element) {
var newURL = element.innerHTML.toLowerCase();

if (newURL === "contact") {
  const main = document.getElementById("content");
  main.innerHTML = "";
  contact();
}

if (newURL === "home") {
  const main = document.getElementById("content");
  main.innerHTML = "";
  home();
}

if (newURL === "menu") {
  const main = document.getElementById("content");
  main.innerHTML = "";
  menu();
}
}