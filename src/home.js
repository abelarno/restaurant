export function home() {
  const content = document.getElementById("content");
  let h1 = document.createElement("h1");
  let h2 = document.createElement("h2");
  let p = document.createElement("p");
  let img = document.createElement("img");

  h1.innerHTML = "🍕 Welcome to The Low Cal Calzone Zone! 🍕";
  h2.innerHTML = "Indulge in Flavor, Without the Guilt!";
  p.innerHTML =
    "At The Low Cal Calzone Zone, we believe that great taste doesn't have to come with a side of guilt. Our mission is to redefine the way you experience the classic calzone by bringing you a menu filled with delicious, mouthwatering options that won't compromise your commitment to a healthy lifestyle.";

  img.setAttribute("src", "../src/lowcalcalzone.png");
  img.setAttribute("width", "200px");

  content.append(h1);
  content.append(h2);
  content.append(p);
  content.append(img);
}
