export function nav() {
  console.log("nav");
  const content = document.getElementById("nav");
  let nav = document.createElement("nav");

  var link1 = document.createElement("button");
  var link2 = document.createElement("button");
  var link3 = document.createElement("button");

  link1.setAttribute("href", "#");
  link1.setAttribute("class", "navButton");
  link1.innerHTML = "Home";

  link2.setAttribute("href", "#");
  link2.setAttribute("class", "navButton");
  link2.innerHTML = "Menu";

  link3.setAttribute("href", "#");
  link3.setAttribute("class", "navButton");
  link3.innerHTML = "Contact";

  nav.appendChild(link1);
  nav.appendChild(link2);
  nav.appendChild(link3);

  content.appendChild(nav);

}
