export function contact() {
  const content = document.getElementById("content");
  let h1 = document.createElement("h1");
  let p = document.createElement("p");

  h1.innerHTML = "Contact";
  p.innerHTML = "Find us everywhere";

  content.append(h1);
  content.append(p);
}
